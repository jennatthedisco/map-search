

// Objecten die niet meteen weergeven mogen worden:
		$(".dropdown-uitklap").hide();
	
		// Topfilter (showing, sort) blijft vast staan 
		window.onscroll = function (event) {
		 $(".topfilter").css('top',$(window).scrollTop());
		}
		
		// Uitklapmenu voor het sort&refine balkje
		$(".dropdown-sort").click(function() {
			$( ".dropdown-uitklap" ).slideToggle(200);
		});
		
		
		
		checkUrl();
		
		function checkUrl(){ // Deze functie zorgt dat de link van de pagina wordt gecontroleerd
			var vars = [], hash;
			// De locatielink wordt gesplit vanaf ? en dan per elke & teken en in variabele gestopt.
			// Vangt alle onderdelen van de url op
			var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); 
			for(var i = 0; i < hashes.length; i++)	{ // Iedere van die variabele
				hash = hashes[i].split('='); 
				vars.push(hash[0]); // Worden in een array gepusht
				vars[hash[0]] = hash[1];
			}
			experienceType = vars.experienceType; // alle resultaten van de array worden met Experiencetype in een global gestopt
			expType(); // Naar volgende functie
		}
		
	
		function expType(){
			if(experienceType === "eat") { // Als de experiencetype eat is 
			// Geef alle experiencetypes die een eat zijn door de juiste link aan te roepen
				experiencesURL = "http://jenniferluttikhuizen.com/Mapsearch/ExperienceDataEat.json";
				
				//experiencesURL = "http://singapore4.withlocals.com/experiences/experience/_search?q=experienceType:eat&size=100"; 
				
			} else if(experienceType === "activity") {
				experiencesURL = "http://jenniferluttikhuizen.com/Mapsearch/ExperienceDataActivity.json";		
				//experiencesURL = "http://singapore4.withlocals.com/experiences/experience/_search?q=experienceType:activity&size=100"; 
				
			} else if(experienceType === "tour") {
				experiencesURL = "http://jenniferluttikhuizen.com/Mapsearch/ExperienceDataTour.json";
				//experiencesURL = "http://singapore4.withlocals.com/experiences/experience/_search?q=experienceType:tour&size=100";
				
			} else {
				experiencesURL = "http://jenniferluttikhuizen.com/Mapsearch/ExperienceData.json";
				//experiencesURL = "http://singapore4.withlocals.com/experiences/experience/_search?pretty=true&size=100";
			}
		}


		
		experiencePins = [];
		// Experiences inladen (size = hoeveel er geladen worden) 
		 $.getJSON(experiencesURL, function(data) { 
			 	parsedJsonfile = data;
				var output = $("<ul />"); // Verschillende list items met experiences erin
				$.each(data.hits.hits, function(key, val) {
					$('<li onclick="openPopupFromCluster(this)" class="experiencesList" id="' + val._id + '">'  // on klik roept li een functie aan en geeft dit li item door
					+ '<div id="Title">' + val._source.title + '</div>' 
					+ '<div id="Type">' + val._source.experienceType + ' Withlocals</div>'  
					+ '<div id="Location"><label>Location:</label> ' + val._source.address.country + '</div><br/>'  
					+ '<div id="Duration"><label>Duration:</label> ' + val._source.durationInt + ' hours</div>'  
					+ '<div id="Price">From <br/> <label>&euro;' + val._source.pricing.advertised_price_origin.amount.value.toFixed(2) + '</label><br/> per person</div>' 
					//to Fixed = 2 getallen achter komma
					+ '<img src="https://res.cloudinary.com/withlocals-com/image/upload/c_fill,f_auto,fl_progressive,g_faces,h_95,r_0.0,w_120/' + val._source.photos[0].id + '.jpg" />' 

					+ '</li>').appendTo(output);
					var LatLgn = [val._source.title, val._source.address.location.lat, val._source.address.location.lon]; 
					experiencePins.push(LatLgn); // De pin locaties in array pushen
	            });
				$('.experiences').html(output); // In experiences div stoppen dus laden op juiste plek
				addExpPins(); // Functie aanroepen
		 });
		
		
		// withlocals.ig0k20dc
		 var map = L.mapbox.map('map', 'mobywan.ik51m6gp', {
			minZoom: 5, // Minimale zoom
			maxZoom: 10, // Maximale zoom
			maxBounds: [ // Wit ruimte wegwerken / bounds zetten
				[-22.85,170.0], // zuid , west
				[35,68.0] // noord, oost
			]	
		}).setView([10.85,100.16], 5); // Middelpunt waar de pagina op start
		
		
		
	
		 
		 //////////// Icoontje voor Eat/tour/act
		 
		 function addExpPins(){
	 		var markers = new L.MarkerClusterGroup(); // Maak een variabele aan dat linkt naar een cluster groep
			allMarkers = [];
			for (var i = 0; i < experiencePins.length; i++) { // Een loop waarin alle markers worden ingeladen 
				numberExperience = i; // Koppel i aan een global
				
				 if (parsedJsonfile.hits.hits[numberExperience]._source.experienceType === "eat"){ // Als de exp type eat is
	
					 var iconExp = new L.icon({ // Maak een icoontje aan
					   iconUrl: 	 'css/IconPin/WLicon2Smallest2EAT.png', // Met deze afbeelding
					   iconSize:     [29, 35],  // Grootte van het icoontje
					   popupAnchor:  [2, -16], // Plek waar de popup moet openen dat bij het icoontje moet

					   className: "pinEat" // Met een classname
					   }),
					   marker = new L.marker ( // Koppel de markers aan de experiences
							[experiencePins[i][1],experiencePins[i][2]], { // Pins toevoegen 1 is eerste getal en 2 is tweede getal in pins array
								icon: iconExp, // De marker heeft het icoon van hierboven
								title: parsedJsonfile.hits.hits[numberExperience]._source.title
							})
							 
							.bindPopup( // Popup content toevoegen in dezelfde stijl als de experiences links
								'<div class="popupContent">'
								+ '<div id="Title">' + parsedJsonfile.hits.hits[numberExperience]._source.title + '</div>'
								+ '<div id="Type">' + parsedJsonfile.hits.hits[numberExperience]._source.experienceType + ' Withlocals</div>'  
								+ '<div id="Location"><label>Location:</label> ' + parsedJsonfile.hits.hits[numberExperience]._source.address.country + '</div><br/>'  
								+ '<div id="Duration"><label>Duration:</label> ' + parsedJsonfile.hits.hits[numberExperience]._source.durationInt + ' hours</div>'  
								+ '<div id="Price">From <br/> <label>&euro;' + parsedJsonfile.hits.hits[numberExperience]._source.pricing.advertised_price_origin.amount.value.toFixed(2) 
								+ '</label><br/> per person</div>' 
								+ '<img src="https://res.cloudinary.com/withlocals-com/image/upload/c_fill,f_auto,fl_progressive,g_faces,h_95,r_0.0,w_120/' // Hierdoor cropt de image
								+ parsedJsonfile.hits.hits[numberExperience]._source.photos[0].id + '.jpg" class="expPic"/>' // afb experience

								+ '<a href="https://www.withlocals.com/experience/' // De knop opent de juiste link van de experience
								+ parsedJsonfile.hits.hits[numberExperience]._source.pretty_url 
								+ '/" class="btn btn-primary trigger">More info</a></div>' // Klikken op button
							);
							
							
					
				 } else if (parsedJsonfile.hits.hits[numberExperience]._source.experienceType === "tour"){
						
					 var iconExp = new L.icon({
					   iconUrl: 	 'css/IconPin/WLicon2Smallest2TOUR.png',
					   iconSize:     [29, 35], 
					   popupAnchor:  [2, -16],
					   className: "pinTour" 
					   }),
					   marker = new L.marker (
							[experiencePins[i][1],experiencePins[i][2]], { 
								icon: iconExp,
								title: parsedJsonfile.hits.hits[numberExperience]._source.title
							})
							  
							.bindPopup( // Popup content toevoegen in de zelfde stijl als de experiences links
								'<div class="popupContent">'
								+ '<div id="Title">' + parsedJsonfile.hits.hits[numberExperience]._source.title + '</div>'
								+ '<div id="Type">' + parsedJsonfile.hits.hits[numberExperience]._source.experienceType + ' Withlocals</div>'  
								+ '<div id="Location"><label>Location:</label> ' + parsedJsonfile.hits.hits[numberExperience]._source.address.country + '</div><br/>'  
								+ '<div id="Duration"><label>Duration:</label> ' + parsedJsonfile.hits.hits[numberExperience]._source.durationInt + ' hours</div>'  
								+ '<div id="Price">From <br/> <label>&euro;' + parsedJsonfile.hits.hits[numberExperience]._source.pricing.advertised_price_origin.amount.value.toFixed(2) 
								+ '</label><br/> per person</div>' 
								+ '<img src="https://res.cloudinary.com/withlocals-com/image/upload/c_fill,f_auto,fl_progressive,g_faces,h_95,r_0.0,w_120/' 
								+ parsedJsonfile.hits.hits[numberExperience]._source.photos[0].id + '.jpg" />'
	
								+ '<a href="https://www.withlocals.com/experience/' 
								+ parsedJsonfile.hits.hits[numberExperience]._source.pretty_url 
								+ '/" class="btn btn-primary trigger">More info</a></div>' // Klikken op button
							);
						
				 } else {
						
					  var iconExp = new L.icon({
					   iconUrl:      'css/IconPin/WLicon2Smallest2ACT.png',
					   iconSize:     [29, 35], 
					   popupAnchor:  [2, -16],
					   className: "pinAct" 
					   }),
					   marker = new L.marker (
							[experiencePins[i][1],experiencePins[i][2]], { 
								icon: iconExp,
								title: parsedJsonfile.hits.hits[numberExperience]._source.title
							})
							  
							.bindPopup( // Popup content toevoegen in de zelfde stijl als de experiences links
								'<div class="popupContent">'
								+ '<div id="Title">' + parsedJsonfile.hits.hits[numberExperience]._source.title + '</div>'
								+ '<div id="Type">' + parsedJsonfile.hits.hits[numberExperience]._source.experienceType + ' Withlocals</div>'  
								+ '<div id="Location"><label>Location:</label> ' + parsedJsonfile.hits.hits[numberExperience]._source.address.country + '</div><br/>'  
								+ '<div id="Duration"><label>Duration:</label> ' + parsedJsonfile.hits.hits[numberExperience]._source.durationInt + ' hours</div>'  
								+ '<div id="Price">From <br/> <label>&euro;' + parsedJsonfile.hits.hits[numberExperience]._source.pricing.advertised_price_origin.amount.value.toFixed(2) 
								+ '</label><br/> per person</div>' 
								+ '<img src="https://res.cloudinary.com/withlocals-com/image/upload/c_fill,f_auto,fl_progressive,g_faces,h_95,r_0.0,w_120/' 
								+ parsedJsonfile.hits.hits[numberExperience]._source.photos[0].id + '.jpg" />'
									
								+ '<a href="https://www.withlocals.com/experience/' 
								+ parsedJsonfile.hits.hits[numberExperience]._source.pretty_url 
								+ '/" class="btn btn-primary trigger">More info</a></div>' // Klikken op button
							);
						
				 }

				markers.addLayer(marker); // Voeg de marker (de losse pins) toe aan de markers (de cluster groep)
				allMarkers.push(marker);
				
			}
			
			
			map.addLayer(markers); // Voeg de markers (de cluster groep) toe aan de map
			$(".pinEat").hide();$(".pinTour").hide();$(".pinAct").hide(); 
			$(".marker-cluster").hide(); // Na het inladen van de data voor de pins maar voor dat ze aan de map worden toegevoegd worden ze gehide voor het eerste zoomlevel
			
		 }
		
		
		
		
		
		// Klikken op land en zoomen op het land
		geojson = L.geoJson(countriesData, { // Haal alle afmetingen van de landen uit de js (WL-countries)
			style: style,
			onEachFeature: onEachFeature // Koppelt naar deze functie
		}).addTo(map); // Alles toevoegen aan de map
		
		function style(e) { // Styling countriesdata
			return {
				weight: 0,
				opacity: 0,
				color: 'transparant',
				dashArray: '',
				fillOpacity: 0,
				fillColor: 0
			}; // anders worden de landen die meegenomen worden in een rare kleur gemaakt om zo te zien dat je erop kan klikken. 
		}
		
		function onEachFeature(feature, layer) {
			layer.on({
				click: zoomToFeature, // Koppelt naar de juiste zoomlevel instellingen na het klikken
			});
		}

			
		function zoomToFeature(e) {	
		//console.log(e.latlng);
			if (map.getZoom() === 5 || map.getZoom() === 6) { // Als na het klikken het zoomlevel 5 of 6 is > Landweergave
				setTimeout(function(){ // Ga in een delay naar 
					 map.setView(e.latlng, 7); // Zoomlevel 7 met de lat lng van waar je klikt > Provincieweergave
				}, 400);
				openPopupFromCluster(this);
			} 
			else if (map.getZoom() === 7 || map.getZoom() === 8) { // > Provincieweergave
				setTimeout(function(){  
					 map.setView(e.latlng, 9); // > Stadweergave
				}, 400);
			}
		}
		 
		 
		 
		 
		function openPopupFromCluster(clickedElement){
			// Cluster uitklappen aanmaken
			var experienceTitle = $(clickedElement).children("#Title")[0].innerHTML;
			
			allMarkers.forEach(function(val,key){
				currentTitle = val.options.title;
				
				if(experienceTitle == currentTitle){
					var spiderable = allMarkers[key].__parent;
					 performTasks(allMarkers[key],spiderable);
				 }
			});
					
		}
		function performTasks(child,parent){
			// als vanuit experiences balk wordt geklikt opent hij in deze view 
			map.setView(parent._latlng, 8);
			setTimeout(function(){ map.setView(parent._latlng, 7)}, 400); 
			setTimeout(function(){ parent.spiderfy()}, 1200); 
			setTimeout(function(){ child.openPopup()}, 2000); 
			
		}
		
		
		
		 
		 
		map.on('zoomend', function() { // Als er zelf gezoomd wordt met de kaart
			if (map.getZoom() >= 7) { // Als het zoomlevel groter of gelijk is aan 7
				$(".pinEat").show();$(".pinTour").show();$(".pinAct").show();$(".marker-cluster").show(); 
				// laat de juiste pins zien
				
			} else { // Anders verstop alle markers
				$(".pinEat").hide();$(".pinTour").hide();$(".pinAct").hide();
				setTimeout(function(){$(".marker-cluster").hide()},5); // Ook de clusterpins
			}
		});